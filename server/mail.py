import smtplib
from email.MIMEMultipart import MIMEMultipart
from email.MIMEText import MIMEText
from configReader import *


class Mail:
    def __init__(self, config, path):
        config = ConfigReader(config, path)
        if config.error != 0:
            print("Bad configuration file\n")
            print(config.error)
            raise
        else:
            self.mail_from = config.mail_from
            self.mail_pass = config.mail_pass
            self.mail_to = config.mail_to
            self.config = config

    def send_mail(self, conteneur_name, value):
        fromaddr = self.mail_from
        toaddr = self.mail_to
        msg = MIMEMultipart()
        msg['From'] = fromaddr
        msg['To'] = toaddr
        msg['Subject'] = "WARNING : Conteneur '" + conteneur_name + "' arrivant au seuil critique"

        body = "Bonjour, \n\n" + "Le conteneur '" + conteneur_name + \
               "' a atteint le seuil critique de remplissage (" + self.config.alert_value + "%). \n" \
               + "Il est remplis a " + str(value) + "%\n\n" + \
               "Bonne journee,\n" + "L'application NFC-Interactive"
        msg.attach(MIMEText(body, 'plain'))

        server = smtplib.SMTP('smtp.gmail.com', 587)
        server.starttls()
        server.login(fromaddr, self.mail_pass)
        text = msg.as_string()

        server.sendmail(fromaddr, toaddr, text)
        server.quit()
