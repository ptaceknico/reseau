#!/usr/bin/python
import re
import os
import codecs


class ConfigReader:
    def __init__(self, conf_file, path):
        self.error = 0
        try:
            fileDir = os.path.dirname(os.path.realpath('__file__'))

            if fileDir != path:
                fileDir = path

            new_file = os.path.join(fileDir, os.path.join('config', conf_file))
            f = codecs.open(new_file)

        except IOError:
            self.error = 1337
        else:
            self.text = f.read()
            f.close()

            match_obj = re.match(r'(?i).*?url\s*:\s*([\w\-.]+).*', self.text, re.S)
            if match_obj:
                self.url = match_obj.group(1)
            else:
                self.error += 5
                print ("Error url\n")

            match_obj = re.match(r'(?i).*?port\s*:\s*(\d+).*', self.text, re.S)
            if match_obj:
                self.port = match_obj.group(1)
            else:
                self.error += 5
                print ("Error port\n")

            match_obj = re.match(r'(?i).*?id\s*:\s*([\w\-.]+).*', self.text, re.S)
            if match_obj:
                self.id = match_obj.group(1)
            else:
                self.error += 5
                print ("Error id\n")

            match_obj = re.match(r'(?i).*?dbname\s*:\s*([\w\-.]+).*', self.text, re.S)
            if match_obj:
                self.dbname = match_obj.group(1)
            else:
                self.error += 5
                print ("Error dbname\n")

            match_obj = re.match(r'(?i).*?pass\s*:\s*([\w\-.@!?*+]+).*', self.text, re.S)
            if match_obj:
                self.passwd = match_obj.group(1)
            else:
                self.error += 5
                print ("Error pass\n")

            match_obj = re.match(r'(?i).*?mail_from\s*:\s*([\w\-.]+).*', self.text, re.S)
            if match_obj:
                self.mail_from = match_obj.group(1)
            else:
                self.error += 5
                print ("Error mail_from\n")

            match_obj = re.match(r'(?i).*?alert_value\s*:\s*([1-9][0-9])', self.text, re.S)
            if match_obj:
                self.alert_value = match_obj.group(1)
            else:
                self.error += 5
                print ("Error alert_value\n")

            match_obj = re.match(r'(?i).*?mail_pass\s*:\s*([\w\-.]+).*', self.text, re.S)
            if match_obj:
                self.mail_pass = match_obj.group(1)
            else:
                self.error += 5
                print ("Error mail_pass\n")

            match_obj = re.match(r'(?i).*?mail_to\s*:\s*([^@]+@[^@]+\.[^@]+)', self.text, re.S)
            if match_obj:
                self.mail_to = match_obj.group(1)
            else:
                self.error += 5
                print ("Error mail_to\n")

            match_obj = re.match(r'(?i).*?crt\s*:\s*([\w\-/.]+.crt).*', self.text, re.S)
            if match_obj:
                self.crt = match_obj.group(1)
            else:
                self.error += 5
                print ("Error certificate\n")

            match_obj = re.match(r'(?i).*?key\s*:\s*([\w\-/.]+.key).*', self.text, re.S)
            if match_obj:
                self.key = match_obj.group(1)
            else:
                self.error += 5
                print ("Error key\n")
