# PFE Webserver

This repo is focused on the server logic. 
The main goal here is to develop Werservices which will be used by the mobile app and the website; this way the processing is done only one time and is available for multiple platforms. 

## Documentation

The webservice provides output as JSON.


### Read only
| Method | URL           | Usage  | Returns  | Auth | Params |
| ------ |-------------  | ------ | ---------| -----| -----|
| GET    | [/](http://jinck.ddns.net:31994)                         | hello_world    | string  | | |
| GET    | [/units](http://jinck.ddns.net:31994/units)              | get all units    | units  | | |
| GET    | [/units/{id}](http://jinck.ddns.net:31994/units/1)       | get an unit and its data   | unit  | | id of the unit|
| GET    | [/units/distance/{id}/{id}](http://jinck.ddns.net:31994/units/distance/1/2)       | get the distance (in km) between two units    | int  | Auth | id_unit, id_unit|
| GET    | [/data](http://jinck.ddns.net:31994/data)                | get all data    | datas  | Auth | |
| GET    | [/data/{id}](http://jinck.ddns.net:31994/data/1)                | get all data form a specific unit ordered by datetime (oldest to newest)| datas  | Auth | id of the unit|
| GET    | [/data/last/{id}](http://jinck.ddns.net:31994/data/last/1)                | get the last data form a specific unit | data  | Auth | id of the unit|

The read API is used client-side to display the datas in our website / mobile app

### Write
| Method | URL           | Usage  | Returns  | Auth | Params | Note |
| ------ |-------------  | ------ | ---------| -----| -------|------|
| GET    | [/data/add?name={unit_name}&value={range_value}&datetime={timestamp}&key={key}](http://jinck.ddns.net:31994/data/add?name=Test1&value=5&datetime=48484848&key=8a863b145dc6e4ed7ac41c08f7536c476ebac7509e028ed2b49f8bd5a3562b9f)                | add a data    | data  | | name, value, timestamp, key| Key is the sha256 of the name; send an email when the filling > 80% |
| POST   | /data/add (x-www-form-urlcoded)                | add a data    | data  | Auth | name, value, timestamp, key| Key is the sha256 of the name; send an email when the filling > 80% |
| DELETE | /data/delete (x-www-form-urlcoded)                | delete a data    | check | Auth | id of the data| |
| DELETE | /data/delete/all (x-www-form-urlcoded)                | delete all the datas of an unit    | checks | Auth | id of the unit| |
| POST   | /units/add (x-www-form-urlcoded)                | add an unit    | unit  | Auth | name, empty_range, lat, lon | |
| POST   | /units/update (x-www-form-urlcoded)                | update an unit    | unit  | Auth | id, name, empty_range, lat, lon | Only 'id' is required other params are optional. If they are not set they won't be updated |
| DELETE | /units/delete (x-www-form-urlcoded)                | delete an unit, all its data and its log    | check  | Auth | id of the unit | |

When writing in the database the request will respond with a field "check". If it's true, the operation has been successful; if it's false an "error" field explain the problem.<br>  
__NB :__ The write access URLs are used by the local units to add their datas in this database

## Set up for you

Install guide for Linux users only <br><br>

<strong>Install Python 2.7</strong><br>
<code>sudo apt-get install python2.7</code>

<strong>Install pip and python dependencies</strong><br>
<code>sudo apt-get install python python-pip</code>

<strong>Install Flask </strong><br>
<code>sudo pip install flask</code><br>
If the command has some problem use <code>sudo easy_install flask</code>

<strong>Install Flask-CORS</strong><br>
Flask-CORS nables CORS support on all routes, for all origins and methods. It allows parameterization of all CORS headers on a per-resource level.<br>
<code>pip install -U flask-cors</code>

<strong>Install MariaDB </strong><br>
<code>sudo apt-get install mariadb-server</code>

<strong>Configure MariaDB</strong><br>
Set up a root password has you like.<br>
Once the install is finished start MariaDB with <code> mysql -u [user] -p</code><br>
Create the database and a user with privileges on it. <strong> The name of the database, the user's name and the password must be set in the file config/config </strong><br>
<code>CREATE DATABASE [name];</code><br>
<code>CREATE USER '[user name]'@'localhost' IDENTIFIED BY '[password]';</code><br>
<code>GRANT ALL PRIVILEGES ON [name].* TO ‘[user name]’@’localhost’;</code>

<strong>Create the tables</strong><br>
In MariaDB run the database script <br>
<code>source sql/server.sql</code><br>
Exit MariaDB with <code>exit</code>

<strong>Update the config file</strong><br>
Update the config file located in the **config** folder with your values.<br>
Update the database values :<br>
<code> url : [url of the database (localhost if using the database on your computeur)]</code><br>
<code> port : [port of the database (3306 if local)]</code><br>
<code> dbname : [name of the database you just created]</code><br>
<code> id : [user name of the user just set]</code><br>
<code> pass : [password of the user you just created]</code><br>
Update the mail infos :<br>
<code> mail_from : [login of the sender mail address]</code><br>
<code> mail_pass : [password of the sender mail address]</code><br>
<code> mail_to : [receiver mail address]</code>

<strong>Install mysql-connector</strong><br>
<code>sudo pip install mysql-connector</code><br>
If the command has some problem use <code>sudo pip install --egg mysql-connector-python-rf</code>

<strong>Generate certificate</strong><br>
Generate a self-signed certificate to run the app in https in a <code>certificates</code> folder or at the root of the project.<br>
<code>openssl req -x509 -sha256 -nodes -days 365 -newkey rsa:2048 -keyout [name].key -out [name].crt</code><br>
Edit the config file if necessary<br>

**NB** : You may have to add the self-signed certificate to your browser; your browser may, otherwise, say that the website isn't secured. 

<strong>Run the app</strong><br>
Once everything is installed run the app <code>python server.py</code><br>
The app is running on your computer at [localhost:31994](http://localhost:31994) and his available for everyone.

## Database

Database files are located under the "sql" folder.<br>
The file <strong>server.sql</strong> creates the server database, all the tables and add some data for exhibit. More infos are available inside the file

## Tech infos

The example is running on my own Raspberry Pi with my own data.

Tech used :
- Python 2.7
- Flask
- MariaDB
- Linux (not working on Windows ATM)

## TO DO

<strong>What's next :</strong>
- ~~CRUD~~
- ~~Standard Authentication~~
- ~~Add some security when writing (API key)~~
- Handle logs

<strong>What should be improved :</strong>
- Make it work on Windows (dbConnect.py line 18-19: encoding problem)       **NOT A PRIORITY**
- ~~Code factorization~~
- Code cleanup
- ~~Use PUT / DELETE~~
