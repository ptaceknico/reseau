DROP DATABASE IF EXISTS pfe_server;

CREATE database pfe_server;
USE pfe_server;

# CREATE TABLES

# Table to store units communicating with the server
CREATE TABLE units (
    id INT(11) AUTO_INCREMENT PRIMARY KEY,
    name VARCHAR(30) UNIQUE NOT NULL,
    hash VARCHAR(65) UNIQUE NOT NULL,
    empty_range INT NOT NULL,
    lat DECIMAL(9,6) NOT NULL,			# GPS lat
    lon DECIMAL(9,6) NOT NULL,			# GPS long
    last_ping DATETIME NOT NULL			# Format YYYY-MM-DD HH:MM:SS
    )ENGINE=InnoDB DEFAULT CHARSET=latin1;

# Table to store data gathered by the units
CREATE TABLE data (
    id INT(11) AUTO_INCREMENT PRIMARY KEY,
    id_unit INT(11) NOT NULL,			# Who gives us the value
    value DECIMAL(5,2) NOT NULL,
    date_time DATETIME NOT NULL,		# Format YYYY-MM-DD HH:MM:SS
    FOREIGN KEY (id_unit) REFERENCES units(id)
    )ENGINE=InnoDB DEFAULT CHARSET=latin1;

# Log value
CREATE TABLE log_list (
	id INT(11) AUTO_INCREMENT PRIMARY KEY,
	value VARCHAR(200) NOT NULL UNIQUE
	)ENGINE=InnoDB DEFAULT CHARSET=latin1;

# Table to store the log
CREATE TABLE log (
    id INT(11) AUTO_INCREMENT PRIMARY KEY,
    id_unit INT(11) NOT NULL,			# Who gives us the value
    id_log INT(11) NOT NULL,			# Which log
    date_time DATETIME NOT NULL,		# Format YYYY-MM-DD HH:MM:SS
    FOREIGN KEY (id_unit) REFERENCES units(id),
    FOREIGN KEY (id_log) REFERENCES log_list(id)
    )ENGINE=InnoDB DEFAULT CHARSET=latin1;

# INSERT VALUES

# Example units
INSERT INTO units (name, hash, empty_range, lat, lon, last_ping)
	VALUES 
	('Test1', '8a863b145dc6e4ed7ac41c08f7536c476ebac7509e028ed2b49f8bd5a3562b9f', '100', '2.221', '2.221', '2017-02-17 00:00:00'),
	('Test2', '32e6e1e134f9cc8f14b05925667c118d19244aebce442d6fecd2ac38cdc97649', '100', '2.222', '2.222', '2017-02-17 00:00:00'),
	('Test3', '68235f4551b9c6423df2af7ead63c90cdd4201ac08525bc3a41cd4755c6c86cb', '85', '2.223', '2.223', '2017-02-17 00:00:00');

# Example data
INSERT INTO data (id_unit, value, date_time) 
	VALUES 
	('1', '52.32', '2017-02-16 00:00:00'),
	('1', '98.32', '2017-02-17 00:00:00'),
	('2', '52.32', '2017-02-16 00:00:00'),
	('2', '52.32', '2017-02-17 00:00:00'),
	('3', '0.32', '2017-02-15 00:00:00'),
	('3', '26.0', '2017-02-16 00:00:00'),
	('3', '100.0', '2017-02-17 00:00:00');

# Example log_list
INSERT INTO log_list (value) 
	VALUES 
	('Error type 1'),
	('Error type 2'),
	('Error type 3');

# Example log
INSERT INTO log (id_unit, id_log, date_time) 
	VALUES 
	('1', '1', '2017-02-16 00:00:00'),
	('1', '2', '2017-02-17 00:00:00'),
	('2', '3', '2017-02-16 00:00:00'),
	('2', '2', '2017-02-17 00:00:00'),
	('3', '2', '2017-02-15 00:00:00'),
	('3', '1', '2017-02-16 00:00:00'),
	('3', '3', '2017-02-17 00:00:00');