import shlex
import subprocess

import ssl
from flask import Flask, jsonify, request
from flask_cors import CORS, cross_origin
from functools import wraps

import logging
import dbConnect
import configReader

app = Flask(__name__)

logging.basicConfig(level=logging.INFO)
logging.getLogger('flask_cors').level = logging.DEBUG

CORS(app, supports_credentials=True)

root_path = app.root_path
db = dbConnect.DBConnect('config', root_path)


def check_auth(username, password):
    return username == 'pfe' and password == 'pfe_server'


def authenticate():
    message = {'message': "Authenticate."}
    resp = jsonify(message)

    resp.status_code = 401
    resp.headers['WWW-Authenticate'] = 'Basic realm="Server"'

    return resp


def requires_auth(f):
    @wraps(f)
    def decorated(*args, **kwargs):
        auth = request.authorization
        if not auth:
            return authenticate()

        elif not check_auth(auth.username, auth.password):
            return authenticate()
        return f(*args, **kwargs)

    return decorated


@app.after_request
def after_request(response):
    response.headers.add('Access-Control-Allow-Origin', '*')
    response.headers.add('Access-Control-Allow-Headers', 'Content-Type,Authorization')
    response.headers.add('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE,OPTIONS')
    return response


@app.route('/')
def hello_world():
    return 'Hello World!'


@app.route('/units')
def api_all_units():
    return db.get_units()


@app.route('/units/<int:unit_id>')
def api_unit(unit_id):
    return db.get_unit(unit_id)


@app.route('/units/distance/<int:unit_id_1>/<int:unit_id_2>')
@requires_auth
def api_unit_distance(unit_id_1, unit_id_2):
    return db.get_distance(unit_id_1, unit_id_2)


@app.route('/units/add', methods=['POST'])
@requires_auth
def api_add_units():

    name = request.form['name']
    empty_range = request.form['empty_range']
    lat = request.form['lat']
    lon = request.form['lon']

    if (name is not None) & (empty_range is not None) & (lat is not None) & (lon is not None):
        return db.add_unit(name, empty_range, lat, lon)
    else:
        return jsonify(error="wrong parameters")


@app.route('/units/update', methods=['POST'])
@requires_auth
def api_update_units():

    id = request.form['id']
    name = request.form.get('name')                 # Optional parameter
    empty_range = request.form.get('empty_range')   # Optional parameter
    lat = request.form.get('lat')                   # Optional parameter
    lon = request.form.get('lon')                   # Optional parameter

    if id is not None:
        return db.update_unit(id, name, empty_range, lat, lon)
    else:
        return jsonify(error="wrong parameters, id is required")


@app.route('/units/delete', methods=['DELETE'])
@requires_auth
def api_delete_unit():

    id = request.form['id']

    if id is not None:
        return db.delete_unit(id)
    else:
        return jsonify(error="wrong parameters")


@app.route('/data')
@requires_auth
def api_all_data():
    return db.get_data()


@app.route('/data/<int:unit_id>')
@requires_auth
def api_one_data(unit_id):
    return db.get_data_id(unit_id)


@app.route('/data/last/<int:unit_id>')
@requires_auth
def api_last_data(unit_id):
    return db.get_data_last(unit_id)


@app.route('/data/add')
def api_add_data_get():

    name = request.args.get('name')
    value = request.args.get('value')
    datetime = request.args.get('datetime')
    key = request.args.get('key')

    if (name is not None) & (value is not None) & (datetime is not None) & (key is not None):
        return db.add_data(name, value, datetime, key)
    else:
        return jsonify(error="wrong parameters")


@app.route('/data/add', methods=['POST'])
@requires_auth
def api_add_data():

    name = request.form['name']
    value = request.form['value']
    datetime = request.form['datetime']
    key = request.form['key']

    if (name is not None) & (value is not None) & (datetime is not None) & (key is not None):
        return db.add_data(name, value, datetime, key)
    else:
        return jsonify(error="wrong parameters")


@app.route('/data/delete', methods=['DELETE'])
@requires_auth
def api_delete_data():

    id = request.form['id']

    if id is not None:
        return db.delete_data(id)
    else:
        return jsonify(error="wrong parameters")


@app.route('/data/delete/all', methods=['DELETE'])
@requires_auth
def api_delete_all_data():

    id_unit = request.form['id']

    if id_unit is not None:
        return db.delete_data_all(id_unit)
    else:
        return jsonify(error="wrong parameters")

if __name__ == '__main__':
    # Run in https
    '''
    config = configReader.ConfigReader('config', root_path)
    context = (config.crt, config.key)
    app.run(host='0.0.0.0', port=31994, ssl_context=context)
    '''

    # Run in std http
    app.run(host='0.0.0.0', port=31994)
