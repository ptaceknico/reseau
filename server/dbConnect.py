#!/usr/bin/python
# encoding=utf8
import collections
import mysql.connector as mariadb
import json
import time
from datetime import datetime
from configReader import *
from processing import *
from mail import *
from flask import jsonify


class DBConnect:
    def __init__(self, config, path):

        self.config = config
        self.path = path

        config = ConfigReader(config, path)
        if config.error != 0:
            print("Bad configuration file\n")
            print(config.error)
            raise
        else:
            self.connection = mariadb.connect(host=config.url, port=config.port, database=config.dbname, user=config.id,
                                              password=config.passwd)
            self.cursor = self.connection.cursor()
            self.configReader = config
            self.query = ""

    def get_units(self):
        self.query = "SELECT id, name, hash, empty_range, lat, lon, last_ping FROM units"
        self.cursor.execute(self.query)
        rows = self.cursor.fetchall()
        objects_list = []

        for row in rows:
            d = collections.OrderedDict()
            d['id'] = row[0]
            d['name'] = row[1]
            d['hash'] = row[2]
            d['empty_range'] = row[3]
            d['lat'] = str(row[4])
            d['lon'] = str(row[5])
            d['last_ping'] = row[6].strftime("%d-%m-%Y %H:%M:%S")
            objects_list.append(d)

        return jsonify(objects_list)

    def get_unit(self, unit_id):

        self.cursor.execute("SELECT COUNT(*) FROM units WHERE id = %s", (int(unit_id),))
        cpt = self.cursor.fetchone()

        if cpt[0] != 0:
            self.cursor.execute("SELECT id, name, hash, empty_range, lat, lon, last_ping FROM units WHERE id = %s", (int(unit_id),))
            res = self.cursor.fetchone()

            objects_list = []

            d = collections.OrderedDict()
            d['id'] = res[0]
            d['name'] = res[1]
            d['hash'] = res[2]
            d['empty_range'] = res[3]
            d['lat'] = str(res[4])
            d['lon'] = str(res[5])
            d['last_ping'] = res[6]

            objects_list2 = []
            self.cursor.execute("SELECT id, value, date_time FROM data WHERE id_unit = %s",
                                (int(unit_id),))
            rows = self.cursor.fetchall()

            for row in rows:
                d2 = collections.OrderedDict()
                d2['id'] = row[0]
                d2['value'] = str(row[1])
                d2['date_time'] = row[2]
                objects_list2.append(d2)

            d['data'] = objects_list2

            objects_list.append(d)
            return jsonify(objects_list)
        else:
            return jsonify(
                error="wrong parameters, make sure the id exists"
            )

    def add_unit(self, name, empty_range, lat, lon):
        check = False

        self.cursor.execute("SELECT COUNT(*) FROM units WHERE name LIKE %s", (name.strip(),))
        res = self.cursor.fetchone()

        if res[0] == 0:     # Check if the name is not already taken
            myhash = sha256(name)
            self.cursor.execute("INSERT INTO units(name, hash, empty_range, lat, lon, last_ping) VALUES (%s, %s, %s, %s, %s, %s)",
                                (name, myhash, int(empty_range), float(lat), float(lon), datetime.now(),))
            self.connection.commit()

            if self.cursor.rowcount == 1:
                check = True

            return jsonify(
                check=check,
                name=name,
                hash=myhash,
                empty_range=empty_range,
                lat=lat,
                lon=lon,
                date_time=datetime.now()
            )

        else:
            return jsonify(
                check=check,
                error="wrong parameters, make sure the name is correct and not already taken"
            )

    def update_unit(self, id, name, empty_range, lat, lon):

        check = False

        self.cursor.execute("SELECT COUNT(*) FROM units WHERE name LIKE %s AND id != %s", (name.strip(), int(id),))
        cpt = self.cursor.fetchone()

        if cpt[0] == 0:     # Check if the name is not already taken by another unit
            self.cursor.execute("SELECT name, empty_range, lat, lon, last_ping FROM units WHERE id = %s", (int(id),))
            res = self.cursor.fetchone()

            if name is None:
                name = res[0]
            if empty_range is None:
                empty_range = res[1]
            if lat is None:
                lat = res[2]
            if lon is None:
                lon = res[3]

            myhash = sha256(name)
            self.cursor.execute("UPDATE units SET name = %s, hash = %s, empty_range = %s, lat = %s, lon = %s WHERE id = %s",
                                (name, myhash, int(empty_range), float(lat), float(lon), int(id),))
            self.connection.commit()

            if self.cursor.rowcount == 1:
                check = True

            return jsonify(
                check=check,
                id=id,
                name=name,
                hash=myhash,
                empty_range=empty_range,
                lat=str(lat),
                lon=str(lon),
                date_time=res[4]
            )

        else:
            return jsonify(
                check=check,
                error="wrong parameters, make sure the name is correct and not already taken"
            )

    def delete_unit(self, id_unit):

        self.cursor.execute("SELECT id FROM data WHERE id_unit = %s", (int(id_unit),))
        rows = self.cursor.fetchall()
        objects_list = []
        objects_list2 = []
        objects_list3 = []

        d = collections.OrderedDict()
        d['id'] = id_unit

        for row in rows:    # Delete all the datas linked to the unit
            check = False

            self.cursor.execute("SELECT id, value, date_time FROM data WHERE id = %s", (row[0],))
            temp = self.cursor.fetchone()

            self.cursor.execute("DELETE FROM data WHERE id = %s", (row[0],))
            self.connection.commit()

            if self.cursor.rowcount == 1:
                check = True

            d2 = collections.OrderedDict()
            d2['id'] = row[0]
            d2['value'] = str(temp[1])
            d2['date_time'] = temp[2]
            d2['check'] = check
            objects_list2.append(d2)

        d['data'] = objects_list2

        self.cursor.execute("SELECT id FROM log WHERE id_unit = %s", (int(id_unit),))
        rows2 = self.cursor.fetchall()

        for row2 in rows2:    # Delete all the logs linked to the unit
            check = False

            self.cursor.execute("DELETE FROM log WHERE id = %s", (row2[0],))
            self.connection.commit()

            if self.cursor.rowcount == 1:
                check = True

            d3 = collections.OrderedDict()
            d3['id'] = row2[0]
            d3['check'] = check
            objects_list2.append(d3)

        d['log'] = objects_list3

        check = False

        # Delete the unit itself
        self.cursor.execute("DELETE FROM units WHERE id = %s", (int(id_unit),))
        self.connection.commit()

        if self.cursor.rowcount == 1:
            check = True

        d['check'] = check
        objects_list.append(d)
        return jsonify(objects_list)

    def get_distance(self, unit_id_1, unit_id_2):
        self.cursor.execute("SELECT lat, lon FROM units WHERE id = %s", (int(unit_id_1),))
        res = self.cursor.fetchone()
        # Lat and long of unit 1
        u1_lat = res[0]
        u1_lon = res[1]

        self.cursor.execute("SELECT lat, lon FROM units WHERE id = %s", (int(unit_id_2),))
        res = self.cursor.fetchone()
        # Lat and long of unit 2
        u2_lat = res[0]
        u2_lon = res[1]

        return jsonify(distance=haversine(u1_lon, u1_lat, u2_lon, u2_lat))

    def get_data(self):
        self.query = "SELECT id, id_unit, value, date_time FROM data"
        self.cursor.execute(self.query)
        rows = self.cursor.fetchall()
        objects_list = []

        for row in rows:
            d = collections.OrderedDict()
            d['id'] = row[0]

            self.cursor.execute("SELECT id, name, empty_range, lat, lon, last_ping FROM units WHERE id = %s", (int(row[1]),))
            res = self.cursor.fetchone()

            d2 = collections.OrderedDict()
            d2['id'] = res[0]
            d2['name'] = res[1]
            d2['empty_range'] = res[2]
            d2['lat'] = str(res[3])
            d2['lon'] = str(res[4])
            d2['last_ping'] = res[5]

            d['id_unit'] = d2

            d['value'] = str(row[2])
            d['date_time'] = row[3]
            objects_list.append(d)

        return jsonify(objects_list)

    def get_data_id(self, id):
        self.cursor.execute("SELECT id, value, date_time FROM data WHERE id_unit = %s ORDER BY date_time", (int(id),))
        rows = self.cursor.fetchall()
        objects_list = []

        for row in rows:
            d = collections.OrderedDict()
            d['id'] = row[0]
            d['value'] = str(row[1])
            d['date_time'] = row[2]
            objects_list.append(d)

        return jsonify(objects_list)

    def get_data_last(self, id):
        self.cursor.execute("SELECT id, value, date_time FROM data WHERE id_unit = %s ORDER BY date_time DESC", (int(id),))
        row = self.cursor.fetchone()

        return jsonify(
            id=row[0],
            value=str(row[1]),
            date_time=row[2]
        )

    def add_data(self, name, value, date_time, key):

        check = False
        self.cursor.execute("SELECT id, empty_range, hash FROM units WHERE name LIKE %s", (name.strip(),))
        res = self.cursor.fetchone()

        if res is None:
            return jsonify(
                check=check,
                error="wrong parameters, make sure the name is correct")
        else:

            if res[2] == key:       # Make sure the key is OK
                id = res[0]
                empty_range = res[1]

                newValue = calculate_range(int(value), empty_range)

                self.cursor.execute("INSERT INTO data(id_unit, value, date_time) VALUES (%s, %s, %s)",
                                    (int(id), newValue, datetime.fromtimestamp(int(date_time)),))
                self.connection.commit()

                if newValue > int(self.configReader.alert_value) :
                    my_mail = Mail(self.config, self.path)
                    my_mail.send_mail(name, newValue)

                if self.cursor.rowcount == 1:
                    check = True
                    self.cursor.execute("UPDATE units SET last_ping = %s WHERE id = %s", (datetime.now(), int(id),))
                    self.connection.commit()

                return jsonify(
                    check=check,
                    id_unit=id,
                    value=newValue,
                    date_time=datetime.fromtimestamp(int(date_time))
                )

            else:
                return jsonify(
                    check=check,
                    error="wrong parameters, make sure the key is correct")

    def delete_data(self, id):
        check = False

        self.cursor.execute("SELECT COUNT(*) FROM data WHERE id = %s", (int(id),))
        cpt = self.cursor.fetchone()

        if cpt[0] != 0:
            self.cursor.execute("DELETE FROM data WHERE id = %s", (int(id),))
            self.connection.commit()

            if self.cursor.rowcount == 1:
                check = True

            return jsonify(
                check=check,
                id=id
            )

        else:
            return jsonify(
                check=check,
                error="wrong parameters, make sure the id is correct"
            )

    def delete_data_all(self, id_unit):

        self.cursor.execute("SELECT id FROM data WHERE id_unit = %s", (int(id_unit),))
        rows = self.cursor.fetchall()
        objects_list = []

        for row in rows:
            check = False

            self.cursor.execute("DELETE FROM data WHERE id = %s", (row[0],))
            self.connection.commit()

            if self.cursor.rowcount == 1:
                check = True

            d = collections.OrderedDict()
            d['id'] = row[0]
            d['check'] = check
            objects_list.append(d)

        return jsonify(objects_list)

    def close(self):
        self.cursor.close()
        self.connection.close()
