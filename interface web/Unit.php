<?php

class Unit
{

    private $id;
    private $name;
    private $lat;
    private $long;
    private $empty_range;
    private $last_ping;

    private $data = array();

    public function __construct($id, $name, $lat, $long, $empty_range, $data, $last_ping)
    {
        $this->id = $id;
        $this->name = $name;
        $this->lat = $lat;
        $this->long = $long;
        $this->empty_range = $empty_range;
        $this->data = $data;
        $this->last_ping = $last_ping;
    }

    public function getLastData()
    {
        if(null !== $this->data && sizeof($this->data) > 0)
            return $this->data[sizeof($this->data) - 1];
        return false;
    }

    public function getLastRange()
    {
        return $this->getLastData()['date_time'];
    }

    public function getLastPing()
    {
        return $this->last_ping;
    }


    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return mixed
     */
    public function getLat()
    {
        return $this->lat;
    }

    /**
     * @param mixed $lat
     */
    public function setLat($lat)
    {
        $this->lat = $lat;
    }

    /**
     * @return mixed
     */
    public function getLong()
    {
        return $this->long;
    }

    /**
     * @param mixed $long
     */
    public function setLong($long)
    {
        $this->long = $long;
    }

    /**
     * @return mixed
     */
    public function getEmptyRange()
    {
        return $this->empty_range;
    }

    /**
     * @param mixed $empty_range
     */
    public function setEmptyRange($empty_range)
    {
        $this->empty_range = $empty_range;
    }

    /**
     * @return array
     */
    public function getData()
    {
        return $this->data;
    }

    /**
     * @param array $data
     */
    public function setData($data)
    {
        $this->data = $data;
    }


}