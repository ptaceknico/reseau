<?php include('includes.php') ; ?>

<!DOCTYPE html>
<html lang="en">
	<head>
		<meta http-equiv="content-type" content="text/html; charset=UTF-8">
		<meta charset="utf-8">
		<title>Tableau de bord</title>
		<meta name="generator" content="Bootply" />
		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
		<link href="css/bootstrap.min.css" rel="stylesheet">
        <!-- Material Design Bootstrap -->
        <link href="css/mdb.min.css" rel="stylesheet">
		<!--[if lt IE 9]>
			<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
		<![endif]-->
		<link href="css/styles.css" rel="stylesheet">

        <style type="text/css">
            ul.line-legend {
                text-align: center;
                margin-top: 2rem;
            }
            ul.line-legend li {
               display:inline-block;
               margin-right: 10px; 
            }
            ul.line-legend li span {
                padding: 5px 10px;
                color: #fff;
            }
        </style>

	</head>
	<body>


    <?php

        $units = json_decode(file_get_contents(API_LINK . 'units'), true);
        $data = array();
        $u = array();

        foreach ($units as $unit) {
            $data = json_decode(file_get_contents(API_LINK . 'data/' . $unit['id'], false, $context), true);
            array_push($u, new Unit($unit['id'], $unit['name'], $unit['lat'], $unit['lon'], $unit['empty_range'], $data, $unit['last_ping']));
        }
    ?>


<!-- header -->
<div id="top-nav" class="navbar navbar-inverse navbar-static-top">
    <div class="container-fluid">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="#">Tableau de bord</a>
        </div>
    </div>
    <!-- /container -->
</div>
<!-- /Header -->

<!-- Main -->
<div class="container-fluid">
    <div class="row">
        <div class="col-sm-1">

        </div>
        <!-- /col-3 -->
        <div class="col-sm-10">

            <!-- column 2 -->
            <a href="#"><strong><i class="glyphicon glyphicon-dashboard"></i> Tableau de bord</strong></a>
            <hr>

            <div class="row">
                <!-- center left-->
                <div class="col-md-6">
                    <div class="well">Nombre de capteurs actifs <span class="badge pull-right"><?php echo sizeof($u); ?></span></div>


                    <div class="btn-group btn-group-justified">
                        <a data-toggle="modal" href="#addWidgetModal" class="btn btn-primary col-sm-3">
                            <i class="glyphicon glyphicon-plus"></i>
                            <br> Ajouter un capteur
                        </a>
                    </div>

                    <hr>

                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h4>Résumé</h4></div>
                        <div class="table-responsive">

                            <table class="table table-striped">
                                <thead>
                                <tr>
                                    <th>Conteneur</th>
                                    <th>Statut</th>
                                    <th>Profondeur</th>
                                    <th>Date</th>
                                    <th>Position</th>
                                    <th></th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php
                                    foreach ($u as $unit) {
                                        /* @var $unit Unit */

                                        $i = 0;
                                        $valuesAsString = "";
                                        $datesAsString = "";

                                        foreach ($unit->getData() as $du) {
                                            $valuesAsString .= $du['value'];
                                            $datesAsString .= $du['date_time'];
                                            $i++;
                                            if ($i < sizeof($unit->getData())) {
                                                $valuesAsString .= ',';
                                                $datesAsString .= ';';
                                            }
                                        }

                                        echo '<tr>';
                                        echo '<td style="cursor:pointer;" data-toggle="modal" href="#chartsModal" data-source="'. $valuesAsString .'" data-labels="' . $datesAsString .'">' . $unit->getName() . ' ('. $unit->getId() . ') </td>';
                                        

                                        if (isset($unit->getLastData()['value']) && !empty($unit->getLastData()['value'])){
                                            if ($unit->getLastData()['value'] > 80){
                                                echo '<td style="cursor:pointer;color:red;" data-toggle="modal" href="#chartsModal" data-source="'. $valuesAsString .'" data-labels="' . $datesAsString .'">' . $unit->getLastData()['value'] . '% </td>'; 
                                            } else {
                                                echo '<td style="cursor:pointer;" data-toggle="modal" href="#chartsModal" data-source="'. $valuesAsString .'" data-labels="' . $datesAsString .'">' . $unit->getLastData()['value'] . '% </td>'; 
                                            } 
                                        } else {
                                            echo '<td style="cursor:pointer;" data-toggle="modal" href="#chartsModal" data-source="'. $valuesAsString .'" data-labels="' . $datesAsString .'"></td>';
                                        }

                                        echo '<td style="cursor:pointer;" data-toggle="modal" href="#chartsModal" data-source="'. $valuesAsString .'" data-labels="' . $datesAsString .'">' . $unit->getEmptyRange(). '</td>';
                                        echo '<td style="cursor:pointer;" data-toggle="modal" href="#chartsModal" data-source="'. $valuesAsString .'" data-labels="' . $datesAsString .'"><b style="font-weight:500;">'. date("H:i", strtotime($unit->getLastPing())).'</b> '.date("d/m", strtotime($unit->getLastPing())).'</td>';
                                        echo '<td style="cursor:zoom-in;" onclick="newLocation(' . $unit->getLat() . ', ' . $unit->getLong() . ')" >' . $unit->getLat() . ', ' . $unit->getLong() . '</td>';
                                        echo '<td style="cursor:pointer;" data-toggle="modal" href="#unitUpdateModal" data-id="'. $unit->getId() .'" data-name="'. $unit->getName() .'" data-lat="'. $unit->getLat() .'" data-lon="'. $unit->getLong() .'" data-range="'. $unit->getEmptyRange() .'"><strong><i class="glyphicon glyphicon-edit"></i></td>';
                                        echo '<td style="cursor:pointer;" data-toggle="modal" href="#unitDeleteModal" data-id="'. $unit->getId() .'"><strong><i style="color:darkred" class="glyphicon glyphicon-remove"></i></td></tr>';
                                    }
                                ?>
                                </tbody>
                            </table>
                        </div>

                    </div>
                    <hr>
                    <!--
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h4>Aperçu</h4></div>
                        <div class="panel-body">
                            <canvas id="myChart"></canvas>
                            <div id="legendDiv"></div>
                        </div>
                    </div>
                    <hr>
                    -->

                </div>

                <!--/col-->
                <div class="col-md-6">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h4>Carte</h4></div>
                        <div class="panel-body">
                            <div id="map" style="width:100%;height:600px;"></div>
                            <br><br>

                            <script>
                                var lat = <?php  echo $u[0]->getLat();  ?>;
                                var lon = <?php  echo $u[0]->getLong();  ?>;
                                var map;

                                function initMap() {
                                    // Create a map object and specify the DOM element for display.
                                    map = new google.maps.Map(document.getElementById('map'), {
                                        center: {lat: lat, lng: lon},
                                        zoom: 10
                                    });

                                    var marker;

                                <?php

                                        foreach ($u as $unit) {
                                            echo "marker = new google.maps.Marker({";
                                            echo "position: new google.maps.LatLng(".$unit->getLat().",".$unit->getLong()."),";
                                            echo "map: map";
                                            echo "});";
                                        }
                                        ?>
                                }

                                function newLocation(newLat,newLng)
                                {
                                    map.setCenter({
                                        lat : newLat,
                                        lng : newLng
                                    });
                                    map.setZoom(16);
                                }

                            </script>
                        </div>
                    </div>
                </div>
                <!--/col-span-6-->

            </div>
            <!--/row-->

        </div>
        <!--/col-span-9-->
    </div>
</div>
<!-- /Main -->

<footer class="text-center">2017 - NFC Interactive</strong></a></footer>

<div class="modal" id="addWidgetModal">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title">Ajouter un capteur</h4>
            </div>
            <div class="modal-body">
                <form>
                    <div class="form-group">
                        <label for="name">Nom: </label>
                        <input type="text" class="form-control" id="addName">
                    </div>
                    <div class="form-group">
                        <label for="lat">Latitude :</label>
                        <input type="text" class="form-control" id="addLat">
                    </div>
                    <div class="form-group">
                        <label for="lon">Longitude :</label>
                        <input type="text" class="form-control" id="addLon">
                    </div>
                    <div class="form-group">
                        <label for="range">Profondeur :</label>
                        <input type="text" class="form-control" id="addRange">
                    </div>
                    <button type="submit" class="btn btn-default addUnit">Ajouter</button>
                </form>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dalog -->
</div>
<!-- /.modal -->

<div class="modal" id="chartsModal">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title">Résumé du conteneur</h4>
            </div>
            <div class="modal-body">
                <canvas id="myChart" width="568" height="300"></canvas>
            </div>
            <div class="modal-footer">
                <a href="#" data-dismiss="modal" class="btn btn-primary">Fermer</a>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dalog -->
</div>


<div class="modal" id="unitDeleteModal">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title">Suppression du capteur</h4>
            </div>
            <div class="modal-body">
                <p>
                    Voulez-vous vraiment supprimer ce capteur ?
                </p>
                <button type="submit" class="btn btn-outline-danger deleteUnit">Supprimer</button>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dalog -->
</div>

<div class="modal" id="unitUpdateModal">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title">Modification du capteur</h4>
            </div>
            <div class="modal-body">
                <form>
                    <div class="form-group">
                        <label for="name">Nom: </label>
                        <input type="text" class="form-control" id="name">
                    </div>
                    <div class="form-group">
                        <label for="lat">Latitude :</label>
                        <input type="text" class="form-control" id="lat">
                    </div>
                    <div class="form-group">
                        <label for="lon">Longitude :</label>
                        <input type="text" class="form-control" id="lon">
                    </div>
                    <div class="form-group">
                        <label for="range">Profondeur :</label>
                        <input type="text" class="form-control" id="range">
                    </div>
                    <button type="submit" class="btn btn-warning updateUnit">Modifier</button>
                </form>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dalog -->
</div>
<!-- /.modal -->
	<!-- script references -->
		<script src="http://ajax.googleapis.com/ajax/libs/jquery/2.0.2/jquery.min.js"></script>
        <script async defer src="https://maps.googleapis.com/maps/api/js?key= AIzaSyBxpnoynKReHOK1VRCY5hNYKrxmMPxW2rg &callback=initMap"></script>
		<script src="js/bootstrap.min.js"></script>
    <!-- MDB core JavaScript -->
    <script type="text/javascript" src="js/mdb.min.js"></script>
		<script src="js/scripts.js"></script>

    <?php

        $all = [];
        $all_dates = [];
        $names = [];

        $i = 0;

        foreach ($u as $unit) {
        /* @var $unit Unit */
                array_push($names, $unit->getName());
                foreach ($unit->getData() as $du) {
                    array_push($all_dates, strtotime($du['date_time']));
                    $all[$i][strtotime($du['date_time'])] = intval($du['value']);
                }
            $i++;
        }

        for ($i =0; $i<sizeof($all); $i++){
            for ($j =0; $j<sizeof($all_dates); $j++){

                if (!array_key_exists($all_dates[$j], $all[$i])){
                    if ($j > 0){
                        $all[$i][$all_dates[$j]] = $all[$i][$all_dates[$j-1]];
                    } else {
                        $all[$i][$all_dates[$j]] = 0;
                    }
                          
                }
            }
        }
        

        for ($i =0; $i<sizeof($all); $i++){
            ksort($all[$i]);
        }

        $string_date = [];
        for ($i =0; $i<sizeof(array_keys($all[0])); $i++){
            array_push($string_date, date("d/m/Y H:i", array_keys($all[0])[$i]));
        }

    ?>

    <script type="text/javascript">
        var option = {
            responsive: true,
        };

        var data = {
        labels: <?php echo json_encode($string_date); ?>,
        datasets: [
            <?php

                for ($i=0; $i<sizeof($all); $i++){

                    $r = rand(1, 255);
                    $g = rand(1, 255);
                    $b = rand(1, 255);
                    
                    echo "{";
                    echo     "label: '".$names[$i]."',";
                    
                    echo     'fillColor: "rgba('.$r.','.$g.','.$b.',0)",';
                    echo     'strokeColor: "rgba('.$r.','.$g.','.$b.',1)",';
                    echo     'pointColor: "rgba('.$r.','.$g.','.$b.',1)",';
                    echo     'pointStrokeColor: "#fff",';
                    echo     'pointHighlightFill: "#fff",';
                    echo     'pointHighlightStroke: "rgba('.$r.','.$g.','.$b.',1)",';
                    echo     'data:'.json_encode($all[$i]);
                    echo '}';
                    
                    if ($i != sizeof($all)){
                        echo ",";
                    }
                    
                }
            ?>
            ]
        };
       
        // Get the context of the canvas element we want to select
        var ctx = document.getElementById("myChart").getContext('2d');
        var myLineChart = new Chart(ctx).Line(data, option);

        document.getElementById("legendDiv").innerHTML = myLineChart.generateLegend();

    </script>

    <script type="text/javascript">
        $(document).ready(function(){

            var chart;
            $('#chartsModal').on('shown.bs.modal',function(event){
                var link = $(event.relatedTarget);
                // get data source
                var source = link.attr('data-source').split(',');
                var labels = link.attr('data-labels').split(';');

                // Chart initialisation
                var modal = $(this);
                var canvas = modal.find('.modal-body canvas');
                var ctx = canvas[0].getContext("2d");
                chart = new Chart(ctx).Line({
                    responsive: true,
                    labels: labels,
                    datasets: [{
                        fillColor: "rgba(151,187,205,0.2)",
                        strokeColor: "rgba(151,187,205,1)",
                        pointColor: "rgba(151,187,205,1)",
                        pointStrokeColor: "#fff",
                        pointHighlightFill: "#fff",
                        pointHighlightStroke: "rgba(151,187,205,1)",
                        data: source
                    }]
                },{});
            }).on('hidden.bs.modal',function(event){
                // reset canvas size
                var modal = $(this);
                chart.destroy();
                var canvas = modal.find('.modal-body canvas');
                canvas.attr('width','568px').attr('height','300px');
                // destroy modal
                $(this).data('bs.modal', null);
            });


            $('#unitUpdateModal').on('shown.bs.modal',function(event){
                var link = $(event.relatedTarget);
                // get data source
                var id = link.attr('data-id');
                var name = link.attr('data-name');
                var lat = link.attr('data-lat');
                var lon = link.attr('data-lon');
                var range = link.attr('data-range');

                var modal = $(this);
                modal.find('.modal-body #name').val(name);
                modal.find('.modal-body #lat').val(lat);
                modal.find('.modal-body #lon').val(lon);
                modal.find('.modal-body #range').val(range);

                $('.updateUnit').click(function(event){
                    event.preventDefault();
                    name = modal.find('.modal-body #name').val();
                    lat = modal.find('.modal-body #lat').val();
                    lon = modal.find('.modal-body #lon').val();
                    range = modal.find('.modal-body #range').val();
                    $.ajax({
                        method: "POST",
                        url: <?php echo '"'.API_LINK2.'"'; ?>+"units/update",
                        headers: {
                            "Authorization": "Basic " + btoa("pfe" + ":" + "pfe_server")
                        },
                        data: { id: id, name: name, empty_range: range, lat: lat, lon: lon}
                    }).done(function(){
                        window.location.reload();
                    });
                });

            });

            $('#unitDeleteModal').on('shown.bs.modal',function(event){
                var link = $(event.relatedTarget);
                // get data source
                var id = link.attr('data-id');

                $('.deleteUnit').click(function(){
                   event.preventDefault();

                    $.ajax({
                        method: "DELETE",
                        url: <?php echo '"'.API_LINK2.'"'; ?>+"units/delete",
                        headers: {
                            "Authorization": "Basic " + btoa("pfe" + ":" + "pfe_server")
                        },
                        data: { id: id}
                    }).done(function(){
                        window.location.reload();
                    });
                });
            });

            $('.addUnit').click(function(event){
                event.preventDefault();
                var name = $('#addName').val();
                var range = $('#addRange').val();
                var lat = $('#addLat').val();
                var lon = $('#addLon').val();

                $.ajax({
                        method: "POST",
                        url: <?php echo '"'.API_LINK2.'"'; ?>+"units/add",
                        headers: {
                            "Authorization": "Basic " + btoa("pfe" + ":" + "pfe_server")
                        },
                        data: { name: name, empty_range: range, lat: lat, lon: lon }
                    }).done(function(){
                        window.location.reload();
                });
            });

        });
    </script>
	</body>
</html>